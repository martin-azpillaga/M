system
{

	foreach entity
	{
		if entity.kinematic
		{
			x = zero
			entity.component = zero
			entity.shader.x = zero
			entity.animator.x = zero
		}
		priority = (zero)
		unary = - zero

		binary = zero + zero
		call = sin(zero)
		mix = zero + entity.mass + entity.animator.speed
	}
}

