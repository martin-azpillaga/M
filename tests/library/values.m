system
{
	proposition = true
	proposition = false

	number = zero
	number = one

	number = epsilon
	number = pi
	number = e
	number = infinity

	number = screen_width
	number = screen_height

	number = last_frame_duration
	number = process_duration
	number = time_speed

	vector = up
	vector = right
	vector = forward

	text = usb_audio_input
	text = hdmi_audio_input
	text = default_audio_input
}
