system
{
	foreach entity
	{
		vector = entity.position
		quaternion = entity.rotation
		vector = entity.scale
		vector = entity.world_position
		quaternion = entity.world_rotation

		proposition = entity.kinematic
		number = entity.mass
		vector = entity.angular_mass
		vector = entity.velocity
		vector = entity.angular_velocity
		vector = entity.force
		vector = entity.angular_force

		vector = entity.box_center
		vector = entity.extents
		vector = entity.sphere_center
		number = entity.radius
		entity_list = entity.collisions

		mesh = entity.mesh
		entity = entity.animator
		animator_controller = entity.animator_controller
		entity = entity.shader
		material = entity.material

		number = entity.near_plane
		number = entity.far_plane
		number = entity.field_of_view
		proposition = entity.perspective
		number = entity.display
		number = entity.viewport_width
		number = entity.viewport_height
		number = entity.viewport_x
		number = entity.viewport_y
		color = entity.background

		color = entity.emission
		image = entity.cookie
		number = entity.cookie_size
		number = entity.intensity
		number = entity.bounce_intensity
		number = entity.spot_angle
		number = entity.range

		audio_clip = entity.audio_clip
		number = entity.volume
		number = entity.pitch
		proposition = entity.loop
		proposition = entity.mute

		vector = entity.destination
		number = entity.traversable_areas
		number = entity.max_speed
		number = entity.max_angular_speed
		number = entity.max_acceleration

		proposition = entity.deathmark
		proposition = entity.presence
	}
}
