system
{
	proposition = true || true
	proposition = true && true

	number = zero | zero
	number = zero & zero
	number = zero ^ zero

	proposition = true == true
	proposition = true != true
	proposition = zero < zero
	proposition = zero <= zero
	proposition = zero >= zero
	proposition = zero > zero

	number = zero << zero
	number = zero >> zero
	number = zero + zero
	number = zero - zero
	number = zero * zero
	number = zero / zero
	number = zero % zero

	vector = up +++ up
	vector = up --- up
	vector = up *** zero
	vector = up /// zero
}
