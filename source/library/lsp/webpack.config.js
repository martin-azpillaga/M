module.exports =
[
	{
		entry: './main.js',
		mode: "none",
		target: 'webworker',
		output: { filename: 'main.js', libraryTarget: 'commonjs' },
		resolve: { fallback: { path: require.resolve('path-browserify') }},
		externals: { vscode: 'commonjs vscode' }
	},
	{
		entry: './server.js',
		target: 'webworker',
		mode: "none",
		output: { filename: 'server.js', libraryTarget: 'var', library: 'serverExportVar' },
		resolve: { fallback: { path: require.resolve('path-browserify'), crypto: require.resolve('crypto-browserify'), stream: require.resolve('stream-browserify') }},
		externals: { vscode: 'commonjs vscode', fs: "commonjs fs" },
	}
];
