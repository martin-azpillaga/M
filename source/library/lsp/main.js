const { LanguageClient } = require("vscode-languageclient/browser");

exports.activate = context => new LanguageClient(null, null, null, { server: new Worker(`${context.extensionUri}/dist/server.js`)});
