const { join } = require("path");

class Generator
{
	constructor (workspace)
	{
		this.workspace = workspace;
		this._base_directory = workspace;
		this.document_changes = [];
	}
	get directory ()
	{
		return this._base_directory;
	}
	set directory (value)
	{
		this._base_directory = join(...value.split("/"));
	}
	create (content, path)
	{
		this.document_changes.push({ kind: "create",	uri: join(this._base_directory, join(...path.split("/"))), options: {overwrite: true} });
		this.document_changes.push({ textDocument: {uri: join(this._base_directory, join(...path.split("/"))), version: null}, edits: [ {	range: {start: {line: 0, character: 0}, end: {line: 0, character: 0}}, newText: content }] })
	}
	remove (path)
	{
		this.document_changes.push({ kind: "delete",	uri: join(this._base_directory, join(...path.split("/"))), options: {recursive: true, ignoreIfNotExists: true } });
	}
}

function indent (strings, ...expressions)
{
	strings = {...strings, 0: strings[0].replace("\n", "")};
	const match = strings[0].match(/^(\t+)/);
	const indentation = match ? match[1] : "";

	let result = "";
	for (let i = 0; i < expressions.length; i++)
	{
		if (expressions[i])
		{
			if (expressions[i].split("\n").length > 1)
			{
				const splitted = strings[i].split("\n");
				const last = splitted[splitted.length-1].replace(indentation, "");
				const extra = last.match(/^\s*/)[0];
				result += strings[i].split("\n").map(x => x.replace(indentation, "")).join("\n") + expressions[i].split("\n").map((x,idx) => idx > 0 ? extra + x:x).join("\n");
			}
			else
			{
				result += strings[i].split("\n").map(x => x.replace(indentation, "")).join("\n") + expressions[i];
			}
		}
		else
		{
			result += strings[i].split("\n").map(x => x.replace(indentation, "")).join("\n").replace(/\s+$/, "");
		}
	}
	result += strings[Object.entries(strings).length-1].split("\n").map(x => x.replace(indentation, "")).join("\n");
	return result;
}

module.exports = { Generator, indent };
