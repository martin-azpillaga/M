exports.Types = class
{
	constructor()
	{
		this.undecided = new Set();
	}

	type (node, semantic_type)
	{
		if (semantic_type === "anything") return;

		if (node.semantic_type === undefined)
		{
			node.semantic_type = semantic_type;
			this.undecided.delete(node);

			for (const neighbor of node.neighbors || [])
			{
				this.type(neighbor, semantic_type);
			}
		}
		else
		{
			if (node.semantic_type !== semantic_type)
			{
				throw [{severity: 1, source: "m", message: `Incompatible types: ${node.semantic_type} & ${semantic_type}`, range: node.position}];
			}
		}
		return true;
	}

	typeArray (nodes, type_array)
	{
		if ( ! Array.isArray(type_array))
		{
			for (let i = 0; i < nodes.length; i++)
			{
				this.type(nodes[i], type_array);
			}
		}
		else
		{
			if (nodes.length !== type_array.length)
			{

				throw [{severity: 1, source: "m", message: `Wrong number of types: ${nodes.length} vs ${type_array.length}`, range: nodes[0].position}];
			}

			for (let i = 0; i < nodes.length; i++)
			{
				this.type(nodes[i], type_array[i]);
			}
		}
		return true;
	}

	bind (a, b)
	{
		if (a === b) return;

		a.neighbors = a.neighbors ? [...a.neighbors, b] : [b];
		b.neighbors = b.neighbors ? [...b.neighbors, a] : [a];

		if (a.semantic_type !== undefined)
		{
			this.type(b, a.semantic_type);
		}
		else if (b.semantic_type !== undefined)
		{
			this.type(a, b.semantic_type);
		}
		else
		{
			this.undecided.add(a);
			this.undecided.add(b);
		}
		return true;
	}

	error (node, visited, errors)
	{
		for (const neighbor of node.neighbors)
		{
			if (!visited.includes(neighbor))
			{
				errors.push(error(neighbor, "Neighbor"));
				visited.push(neighbor);
				this.error(neighbor, visited, errors);
			}
		}
	}

	report ()
	{
		const first = [...this.undecided][0];
		if (first !== undefined)
		{
			throw [{message: "Undecidable type", severity: 1, source: "m", range: first.position}];
		}
	}
}
