const { indent } = require("../../library/generation/generator");
const { library } = require("../standard_library");
const { type, library_component, binary_operator, unary_operator, map, value, inspector_field } = require("./library");
const crypto = require("crypto");

function reflection (systems, text)
{
	const lines = text.split("\n");
	let current_system;
	let current_code;
	const source_code = {};
	for (var i = 0; i < lines.length; i++)
	{
		const line = lines[i];
		if (line.match(/^[a-z_]+( shader)?$/))
		{
			if (current_system)
			{
				source_code[current_system] = current_code;
			}
			current_system = line;
			current_code = line;
		}
		else
		{
			current_code += String.raw`\n${line.replaceAll("\t", "\\t")}`;
		}
	}
	source_code[current_system] = current_code;

	const system_declarations = [];
	for (const system of systems)
	{
		var name = system.name.text;

		system_declarations.push(`
		new System{Name = "${name}", SourceCode = "${source_code[name]}", Roles = new List<Role>
		{
			${Object.keys(system.roles).map(role =>
				`new Role { Name = "${role}", Components = new List<Type>{ ${Array.from(system.roles[role]).map(c => `typeof(${library_component(c)[0] || c})`).join(", ")}} }`).join(",\n")}
		}}`)
	}
	return indent`
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	namespace M
	{
		public class Reflection : MonoBehaviour
		{
			public static List<System> Systems = new List<System>
			{
				${system_declarations.join(",\n")}
			};
		}
	}`;
}
function component (name, t)
{
	const awake = t === "input" ? "void Awake () { Value.Enable(); }" : undefined;
	const update = t === "input" ? "void Update () { SystemRunner.SendInput(Value); }" : undefined;

	return indent`
	using UnityEngine;
	using Unity.Mathematics;
	using static Unity.Mathematics.math;
	using UnityEngine.InputSystem;
	using System.Collections.Generic;

	namespace M
	{
		[DisallowMultipleComponent]
		[AddComponentMenu("")]
		public class ${name} : MonoBehaviour
		{
			public ${type(t)} Value;
			${awake}
			${update}

			#if UNITY_EDITOR
			[UnityEditor.CustomEditor(typeof(${name}))]
			public class ${name}Inspector : UnityEditor.Editor
			{
				void OnEnable ()
				{
					target.hideFlags = HideFlags.HideInInspector;
				}
			}
			#endif
		}
	}`;
}
function component_metadata(name)
{
	const uuid = crypto.createHash("sha1").update(name).digest("hex").substring(0, 32);
	return indent`
	fileFormatVersion: 2
	guid: ${uuid}
	`;
}
function role(system_name, name, components, all_components)
{
	const named_components = [];
	for (const component of components)
	{
		named_components.push(library_component(component)[0] || component);
	}
	const requirements = named_components.map(x => `[RequireComponent(typeof(${x}))]`).join("\n");

	const declarations = [];
	for (const component of components)
	{
		const [unity_component, field] = library_component(component);
		if (unity_component)
		{
			declarations.push(`go.GetComponent<${unity_component}>().${field} = UnityEditor.EditorGUILayout.${inspector_field(library.component[component].semantic_type)}Field("${component}", go.GetComponent<${unity_component}>().${field});`);
		}
		else
		{
			declarations.push(`go.GetComponent<${component}>().Value = UnityEditor.EditorGUILayout.${inspector_field(all_components[component])}Field("${component}", go.GetComponent<${component}>().Value);`);
		}
	}
	return indent`
	using UnityEngine;

	namespace M
	{
		[DisallowMultipleComponent]
		${requirements}
		public class ${system_name}_${name} : MonoBehaviour
		{

		}

		#if UNITY_EDITOR
		[UnityEditor.CustomEditor(typeof(${system_name}_${name}))]
		public class ${system_name}_${name}_editor : UnityEditor.Editor
		{
			public override void OnInspectorGUI ()
			{
				var go = target as ${system_name}_${name};
				${declarations.join("\n")}
			}
		}
		#endif
	}
	`;
}
function system (program)
{
	const initialize_roles = Object.keys(program.roles).map(role =>
	`static List<${role}_role> ${role}_list = new List<${role}_role>();`
	).join("\n");

	const component_set = {};

	for (const role in program.roles)
	{
		component_set[role] = new Set();

		for (const component of program.roles[role])
		{
			component_set[role].add(library_component(component)[0] || component);
		}
	}

	const declare_roles = Object.keys(program.roles).map(role => indent
	`
	struct ${role}_role
	{
		public GameObject _go;
		${[...component_set[role]].map(x => indent`public ${x} ${x};`).join("\n")}
	}`
	).join("\n");

	const on_create = Object.keys(program.roles).map(role => indent
	`
	if (${[...component_set[role]].map(x => `go.GetComponent<${x}>() is var ${role}_${x} && ${role}_${x}`).join(" && ")})
	{
		${role}_list.Add(new ${role}_role {_go = go, ${[...component_set[role]].map(x => `${x} = ${role}_${x}`).join(", ")}});
	}`
	).join("\n");

	const on_destroy = Object.keys(program.roles).map(role => indent
	`
	for (var i = ${role}_list.Count - 1; i >= 0; i--)
	{
		if (${role}_list[i]._go == go)
		{
			${role}_list.RemoveAt(i);
		}
	}`
	).join("\n");

	return indent`
	using UnityEngine;
	using UnityEngine.AI;
	using UnityEngine.InputSystem;
	using Unity.Mathematics;
	using static Unity.Mathematics.math;
	using System.Linq;
	using System.Collections.Generic;

	namespace M
	{
		public static class ${program.name.text}
		{
			${initialize_roles}
			${declare_roles}

			[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
			static void Initialize()
			{
				Runner.Initialize();
				Runner.OnEntityCreated += OnCreate;
				Runner.OnEntityDestroyed += OnDestroy;
				Runner.OnFrameUpdate += OnUpdate;
			}
			static void OnDestroy (GameObject go)
			{
				${on_destroy}
			}
			static void OnCreate (GameObject go)
			{
				${on_create}
			}
			static void OnUpdate ()
			{
				${(program.statements||[]).map(statement).join("\n")}
			}
		}
	}`;
}
function statement (statement)
{
	switch (statement.syntactic_type)
	{
		case "iteration": return iteration(statement);
		case "selection": return selection(statement);
		case "assignment": return assignment(statement);
	}
}
function iteration (iteration)
{
	let result;

	const iterator = iteration.iterator.text;

	if (iteration.collection)
	{
		result = indent`
		var ${iterator}_collection = ${expression(iteration.collection)};
		for (var ${iterator}_i = ${iterator}_list.Count - 1; ${iterator}_i >= 0; ${iterator}_i--)
		{
			var ${iterator} = ${iterator}_list[${iterator}_i];
			if (${iterator}_collection.Contains(${iterator}._go))
			{
				${(iteration.statements||[]).map(statement).join("\n")}
			}
		}`;
	}
	else
	{
		result = indent`
		for (var ${iterator}_i = ${iterator}_list.Count - 1; ${iterator}_i >= 0; ${iterator}_i--)
		{
			var ${iterator} = ${iterator}_list[${iterator}_i];
			${(iteration.statements||[]).map(statement).join("\n")}
		}`;
	}

	return result;
}
function selection (selection)
{
	return indent`
	if (${expression(selection.condition)})
	{
		${(selection.statements||[]).map(statement).join("\n")}
	}`;
}
function assignment (assignment)
{
	if (assignment.address.components)
	{
		if (assignment.address.components[0].text === "shader")
		{
			const entity = assignment.address.root.text;
			const field = assignment.address.components[1].text;

			const map = {vector: "Vector", number: "Float", color: "Color", image: "Texture"};
			const field_type = map[assignment.address.components[1].semantic_type];

			const cast = field_type === "Vector" ? "(Vector3) " : "";

			system.roles[entity] = system.roles[entity] || new Set();
			system.roles[entity].add("SkinnedMeshRenderer");

			return `${entity}.SkinnedMeshRenderer.material.Set${field_type}("${field}", ${cast}${expression(assignment.expression)});`;
		}
		else
		{
			return indent`${address(assignment.address)} = ${expression(assignment.expression)};`
		}
	}
	else
	{
		if (assignment.address.declaration)
		{
			return indent`var ${address(assignment.address)} = ${expression(assignment.expression)};`
		}
		else
		{
			return indent`${address(assignment.address)} = ${expression(assignment.expression)};`
		}
	}
}
function expression (expression)
{
	switch (expression.syntactic_type)
	{
		case "priority": return priority(expression);
		case "binary": return binary(expression);
		case "unary": return unary(expression);
		case "functional": return functional(expression);
		case "address": return address(expression);
	}
}
function priority (priority)
{
	return indent`(${expression(priority.expression)})`;
}

function binary (binary)
{
	if (binary.operator.text == "@")
	{
		return indent`${expression(binary.right)}.Contains(${expression(binary.left)})`
	}
	else if (["|", "&", "^", "<<", ">>"].includes(binary.operator.text))
	{
		return indent`(float) ((int)${expression(binary.left)} ${binary.operator.text} (int)${expression(binary.right)})`
	}

	return indent`${expression(binary.left)} ${binary_operator(binary.operator.text)} ${expression(binary.right)}`;
}
function unary (unary)
{
	if (unary.operator.text == "~")
	{
		return indent`((float) (int)${expression(unary.expression)})`;
	}
	return indent`${unary_operator(unary.operator.text)} ${expression(unary.expression)}`;
}
function functional (functional)
{
	return map(functional.name.text, (functional.arguments||[]).map(expression));
}
function address (address)
{
	const entity = address.root.text;

	let result = entity;
	if (address.components)
	{
		for (const component of address.components)
		{
			const component_name = component.text;

			const [unity_component, field] = library_component(component_name);

			if (address.created_entity)
			{
				if (unity_component)
				{
					result += `.GetComponent<${unity_component}>().${field}`;
				}
				else
				{
					result += `.GetComponent<${component_name}>().Value`;
				}
			}
			else
			{
				if (unity_component)
				{
					result += `.${unity_component}.${field}`;
				}
				else
				{
					result += `.${component_name}.Value`;
				}
			}
		}
	}
	else if (value(address.root.text) !== undefined)
	{
		return value(address.root.text);
	}
	else if (address.semantic_type === "entity" && ! address.created_entity)
	{
		result += `._go`;
	}

	return result;
}

function shader (program)
{
	const type_map = {color: "Color", number: "Float", image: "2D"};
	const hlsl_type = {color: "float4", number: "float", image: "sampler2D"};
	const default_value = {color: "(1,1,1,1)", number: "0", image: `"white" {}`};

	const material_properties = [];
	const hlsl_properties = [];

	for (const [name, type] of Object.entries(program.properties))
	{
		material_properties.push(`MAT_${name}("${name.replace("_", " ")}", ${type_map[type]}) = ${default_value[type]}`);
		hlsl_properties.push(`${hlsl_type[type]} MAT_${name};`);
	}

	const vertex_inputs = [];
	const vertex_outputs = [];
	const fragment_inputs = [];
	const fragment_outputs = [];
	const interpolations = [];

	const builtin_interpolations =
	{
		position: "OUT_position = UnityObjectToClipPos(IN_position);",
		texture_zero: "OUT_texture_zero = IN_texture_zero;"
	};
	const builtin =
	{
		position: ["float4", "position", "POSITION"],
		texture_zero: ["float4", "texture_zero", "TEXCOORD0"],
		color: ["fixed4", "albedo", "SV_TARGET"],
	}

	for (const [name, custom] of Object.entries(program.interpolators))
	{
		vertex_inputs.push(builtin[name]);
		vertex_outputs.push(builtin[name]);
		fragment_inputs.push(builtin[name]);
		if (!custom)
		{
			interpolations.push(builtin_interpolations[name]);
		}
	}

	if (!program.lit)
	{
		fragment_outputs.push(builtin.color);
	}

	return indent`
	Shader "M/${program.name.text}"
	{
		Properties
		{
			${material_properties.join("\n")}
		}
		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
			}
			Pass
			{
				AlphaToMask Off
				Blend SrcAlpha OneMinusDstAlpha
				BlendOp Add
				ColorMask RGB
				Conservative False
				Cull Off
				Offset 0, 0
				Stencil { Comp always Pass keep }
				ZClip Off
				ZTest Always
				ZWrite On

				HLSLPROGRAM
				#pragma vertex vertex_shader
				#pragma fragment fragment_shader
				#include "UnityCG.cginc"

				${hlsl_properties.join("\n")}

				void vertex_shader(${[...vertex_inputs.map(([type, name, semantic]) => `${type} IN_${name} : ${semantic}`), ...vertex_outputs.map(([type, name, semantic]) => `out ${type} OUT_${name} : ${semantic}`)].join(", ")})
				{
					${interpolations.join("\n")}
				}

				void fragment_shader(${[...fragment_inputs.map(([type, name, semantic]) => `${type} IN_${name} : ${semantic}`), ...fragment_outputs.map(([type, name, semantic]) => `out ${type} OUT_${name} : ${semantic}`)].join(", ")})
				{
					${program.statements?.map(gpu_statement).join("\n")}
				}
				ENDHLSL
			}
		}
	}`;
}
function gpu_statement(statement)
{
	return `${gpu_expression(statement.address)} = ${gpu_expression(statement.expression)};`;
}
function gpu_expression(expression)
{
	const hlsl_name = {sample: "tex2D", step: "step"};

	switch(expression.syntactic_type)
	{
		case "gpu_priority": return `(${gpu_expression(expression.expression)})`;
		case "gpu_unary": return `${unary_operator(expression.operator)}${gpu_expression(expression.expression)}`;
		case "gpu_binary": return `${gpu_expression(expression.left)} ${binary_operator(expression.operator)} ${gpu_expression(expression.right)}`;
		case "gpu_functional": return `${hlsl_name[expression.name.text]}(${expression.arguments.map(gpu_expression).join(", ")})`;
		case "gpu_material_address": return `MAT_${expression.component.text}`;
		case "gpu_vertex_address": return `IN_${expression.component.text}`;
		case "gpu_fragment_address": return `${library["fragment component"][expression.component.text]?.output ? "OUT" : "IN"}_${expression.component.text}`;
		case "gpu_value": return `${expression.name.text}`;
	}
}

module.exports = { system, shader, component, role, component_metadata, reflection };
