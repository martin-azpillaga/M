const { proposition, number, vector, quaternion, text, mesh, material, image, color, audio_clip, animator_controller, entity, entity_list, curve, input } = require("../standard_library").types;

function type (type)
{
	switch (type)
	{
		case proposition: return "bool";
		case number: return "float";
		case vector: return "Vector3";
		case quaternion: return "Quaternion";
		case text: return "string";
		case mesh: return "Mesh";
		case material: return "Material";
		case image: return "Image";
		case color: return "Color";
		case audio_clip: return "AudioClip";
		case animator_controller: return "RuntimeAnimatorController";
		case entity: return "GameObject";
		case entity_list: return "List<GameObject>";
		case curve: return "AnimationCurve";
		case input: return "InputAction";
	}
}
function inspector_field (type)
{
	switch (type)
	{
		case proposition: return "bool";
		case number: return "Float";
		case vector: return "Vector3";
		case quaternion: return "Quaternion";
		case text: return "string";
		case mesh: return "Mesh";
		case material: return "Material";
		case image: return "Image";
		case color: return "Color";
		case audio_clip: return "AudioClip";
		case animator_controller: return "RuntimeAnimatorController";
		case entity: return "GameObject";
		case entity_list: return "List<GameObject>";
		case curve: return "AnimationCurve";
		case input: return "InputAction";
	}
}
function value (value)
{
	switch (value)
	{
		case "true": return "true";
		case "false": return "false";

		case "zero": return "0f";
		case "one": return "1f";
		case "epsilon": return "math.EPSILON";
		case "pi": return "math.PI";
		case "e": return "math.E";
		case "infinity": return "math.INFINITY";
		case "up": return "Vector3.up";
		case "right": return "Vector3.right";
		case "forward": return "Vector3.forward";
		case "default_audio_input": return "null";
		case "usb_audio_input": return "Microphone.devices.Where(x => x.Contains(\"USB\")).First()";
		case "hdmi_audio_input": return "Microphone.devices.Where(x => x.Contains(\"HDMI\")).First()";
		case "last_frame_duration": return "Time.deltaTime";
		case "process_duration": return "Time.time";
		case "time_speed": return "Time.timeScale";
		case "screen_width": return "Screen.width";
		case "screen_height": return "Screen.height";
		case "triggered": return "triggered";
	}
}
function library_component (component)
{
	switch (component)
	{
		case "listening_ip": return ["Server", "ip"];
		case "listening_port": return ["Server", "port"];
		case "connecting_ip": return ["Client", "ip"];
		case "connecting_port": return ["Client", "port"];

		case "position": return ["Transform", "localPosition"];
		case "rotation": return ["Transform", "localRotation"];
		case "scale": return ["Transform", "localScale"];
		case "world_position": return ["Transform", "position"];
		case "world_rotation": return ["Transform", "rotation"];

		case "kinematic": return ["Rigidbody", "isKinematic"];
		case "mass": return ["Rigidbody", "mass"];
		case "angular_mass": return ["Rigidbody", "inertiaTensor"];
		case "velocity": return ["Rigidbody", "velocity"];
		case "angular_velocity": return ["Rigidbody", "angularVelocity"];

		case "box_center": return ["BoxCollider", "center"];
		case "extents": return ["BoxCollider", "size"];
		case "sphere_center": return ["SphereCollider", "center"];
		case "radius": return ["SphereCollider", "radius"];
		case "collisions": return ["collisions", "Value"];

		case "mesh": return ["SkinnedMeshRenderer", "sharedMesh"];
		case "material": return ["SkinnedMeshRenderer", "material"];

		case "near_plane": return ["Camera", "nearClipPlane"];
		case "far_plane": return ["Camera", "farClipPlane"];
		case "field_of_view": return ["Camera", "fieldOfView"];
		case "perspective": return ["Camera", "orthographic"];
		case "display": return ["Camera", "targetDisplay"];
		case "viewport": return ["Camera", "rect"];
		case "background": return ["Camera", "backgroundColor"];

		case "emission": return ["Light", "color"];
		case "cookie": return ["Light", "cookie"];
		case "cookie_size": return ["Light", "cookieSize"];
		case "intensity": return ["Light", "intensity"];
		case "bounce_intensity": return ["Light", "bounceIntensity"];
		case "range": return ["Light", "range"];
		case "spot_angle": return ["Light", "spotAngle"];

		case "audio_clip": return ["AudioSource", "clip"];
		case "volume": return ["AudioSource", "volume"];
		case "pitch": return ["AudioSource", "pitch"];
		case "loop": return ["AudioSource", "loop"];
		case "mute": return ["AudioSource", "mute"];

		case "destination": return ["NavMeshAgent", "destination"];
		case "max_acceleration": return ["NavMeshAgent", "acceleration"];
		case "max_angular_speed": return ["NavMeshAgent", "angularSpeed"];
		case "max_speed": return ["NavMeshAgent", "speed"];
		case "traversable_areas": return ["NavMeshAgent", "areaMask"];

		case "animator_controller": return ["Animator", "runtimeAnimatorController"];

		default: return [];
	}
}
function map (name, [x, y, z, w])
{
	switch (name)
	{
		case "accept_connection": return `SystemRunner.AcceptConnection(${x})`;
		case "abs":
		case "sin":
		case "cos":
		case "tan":
		case "asin":
		case "acos":
		case "atan":
		case "exp":
		case "sqrt":
		case "degrees":
		case "radians":
		case "length":
		case "log": return `${name}(${x})`;

		case "max":
		case "min":
		case "dot":
		case "distance":
		case "cross":
		case "pow": return `math.${name}(${x}, ${y})`;

		case "xyz": return `new Vector3(${x}, ${y}, ${z})`;
		case "rgba": return `new Color(${x}, ${y}, ${z}, ${w})`;

		case "x":
		case "y":
		case "z": return `${x}.${name}`;

		case "red": return `${x}.r`;
		case "green": return `${x}.g`;
		case "blue": return `${x}.b`;
		case "alpha": return `${x}.a`;

		case "input_number": return `SystemRunner.ReadInput(${x}, ${y})`;
		case "input_triggered": return `SystemRunner.ReadInput(${x}, ${y}) == 1`;

		case "integer_part": return `math.trunc(${x})`;
		case "random": return `UnityEngine.Random.Range(${x}, ${y})`;
		case "spherical_interpolation": return `math.slerp(${x}, ${y}, ${z})`;

		case "quaternion": return `Quaternion.Euler(${x})`;
		case "vector": return `${x}.eulerAngles`;
		case "viewport": return `(${y}).GetComponent<Camera>().WorldToViewportPoint(${x})`;
		case "world": return `(${y}).GetComponent<Camera>().ViewportToWorldPoint(${x})`
		case "text": return `Convert.ToString(${x})`;
		case "number": return `float.Parse(${x})`

		case "create": return `SystemRunner.Create(${x})`;
		case "evaluate": return `${x}.Evaluate(${y})`;

		case "ray": return `new List<RaycastHit>(Physics.RaycastAll(${x}, ${y}, ${z}))`;
		case "box": return `new List<RaycastHit>(Physics.BoxCastAll(${x}, ${y}, Vector3.zero, ${z}, 0))`;
		case "sphere": return `new List<RaycastHit>(Physics.SphereCastAll(${x}, ${y}, Vector3.zero))`;

		case "save": return `SystemRunner.Save(${x})`;
		case "load": return `SystemRunner.Load(${x})`;
	}
}
function unary_operator (operator)
{
	switch (operator)
	{
		case "!": return "!";
		case "~": return "(float) ~ (int)";
		case "-": return "-";
	}
}
function binary_operator (operator)
{
	switch (operator)
	{
		case "||":
		case "&&":
		case "|":
		case "&":
		case "^":
		case "==":
		case "!=":
		case "<":
		case "<=":
		case ">=":
		case ">":
		case "<<":
		case ">>":
		case "+":
		case "-":
		case "*":
		case "/":
		case "%": return operator;

		case "+++": return "+";
		case "---": return "-";
		case "***": return "*";
		case "///": return "/";
	}
}

module.exports = { type, inspector_field, value, library_component, map, unary_operator, binary_operator };
