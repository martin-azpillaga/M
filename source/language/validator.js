const { Lexer } = require("../library/validation/lexer")
const { Scope } = require("../library/validation/scope");
const { Types } = require("../library/validation/types");
const { categories: a, types: {proposition, entity, entity_list} } = require("../language/standard_library");

exports.validate = text =>
{
	const lexer = new Lexer(text);
	const scope = new Scope();
	const types = new Types();

	const identifier = lexer.token(/[\p{Lowercase_Letter}_]+/uy, "identifier");
	const operator = lexer.token(/[#@\|&\^~!=<>+\-*\/%]+/y, "operator");
	const keyword_iteration = lexer.token(/foreach/y);
	const keyword_selection = lexer.token(/if/y);
	const keyword_shader = lexer.token(/shader/y);
	const keyword_begin = lexer.token(/{/y);
	const keyword_assign = lexer.token(/=/y);
	const keyword_end = lexer.token(/}/y);
	const keyword_open = lexer.token(/\(/y);
	const keyword_next = lexer.token(/,/y);
	const keyword_close = lexer.token(/\)/y);
	const keyword_access = lexer.token(/\./y);
	const keyword_at = lexer.token(/@/y);
	const keyword_material = lexer.token(/material/y);
	const keyword_vertex = lexer.token(/vertex/y);
	const keyword_fragment = lexer.token(/fragment/y);
	const _ = true;

	lexer.ghost_rules = [program, statement, expression, gpu_assignment_address, gpu_expression, gpu_address];

	const tree = lexer.read(file);
	const leaves = lexer.leaves;

	lexer.report();
	scope.report();
	types.report();

	return { tree, leaves };

	function file (node)
	{
		return lexer.loop(_ => lexer.push("programs", program));
	}
	function program (node)
	{
		return (
		lexer.read(system) ||
		lexer.read(shader));
	}
	function system (node)
	{
		return (
		lexer.assign("name", identifier) &&
		scope.define(a.system, node.name) &&
		lexer.read(keyword_begin) &&
		scope.push() &&
		lexer.loop(_ => lexer.push("statements", statement)) &&
		scope.pop() &&
		lexer.read(keyword_end));
	}
	function statement (node)
	{
		return (
		lexer.read(iteration) ||
		lexer.read(selection) ||
		lexer.read(assignment));
	}
	function iteration (node)
	{
		return (
		lexer.read(keyword_iteration) &&
		lexer.assign("iterator", identifier) &&
		lexer.optional(_ => lexer.read(keyword_at) && lexer.assign("collection", expression) && types.type(node.collection, entity_list)) &&
		lexer.read(keyword_begin) &&
		scope.push() &&
		scope.define(a.value, node.iterator) &&
		types.type(node.iterator, entity) &&
		lexer.loop(_ => lexer.push("statements", statement)) &&
		scope.pop() &&
		lexer.read(keyword_end));
	}
	function selection (node)
	{
		return (
		lexer.read(keyword_selection) &&
		lexer.assign("condition", expression) &&
		types.type(node.condition, proposition, "condition of selection") &&
		lexer.read(keyword_begin) &&
		scope.push() &&
		lexer.loop(_ => lexer.push("statements", statement)) &&
		scope.pop() &&
		lexer.read(keyword_end));
	}
	function assignment (node)
	{
		return (
		lexer.assign("address", address) &&
		lexer.read(keyword_assign) &&
		lexer.assign("expression", expression) &&
		types.bind(node.address, node.expression));
	}
	function expression (node)
	{
		return (
		lexer.read(priority) ||
		lexer.read(unary) ||
		lexer.read(binary) ||
		lexer.read(functional) ||
		lexer.read(address));
	}
	function priority (node)
	{
		return (
		lexer.read(keyword_open) &&
		lexer.assign("expression", expression) &&
		lexer.read(keyword_close) &&
		types.bind(node, node.expression, "priority expression"));
	}
	function unary (node)
	{
		return (
		lexer.assign("operator", operator) &&
		lexer.assign("expression", expression) &&
		scope.access(a.unary_operator, node.operator, definition => types.typeArray([node, node.expression], [definition.return_type, ...definition.parameters.map(x => x.type)])));
	}
	function binary (node)
	{
		return (
		(lexer.assign("left", priority) || lexer.assign("left", unary) || lexer.assign("left", functional) || lexer.assign("left", address)) &&
		lexer.assign("operator", operator) &&
		lexer.assign("right", expression) &&
		scope.access(a.binary_operator, node.operator, definition => types.typeArray([node, node.left, node.right], [definition.return_type, ...definition.parameters.map(x => x.type)])));
	}
	function functional (node)
	{
		return (
		lexer.assign("name", identifier) &&
		lexer.read(keyword_open) &&
		lexer.optional(_ => lexer.push("arguments", expression) && lexer.loop(_ => lexer.read(keyword_next) && lexer.push("arguments", expression))) &&
		lexer.read(keyword_close) &&
		scope.access(a.map, node.name, definition => types.typeArray([node, ...(node.arguments||[])], [definition.return_type, ...definition.parameters.map(x => x.type)])));
	}
	function address (node)
	{
		return (
		lexer.assign("root", identifier) &&
		lexer.loop(_ => lexer.read(keyword_access) && lexer.push("components", identifier) && scope.accessOrDefineGlobally(a.component, node.components[node.components.length - 1], definition => types.bind(node.components[node.components.length-1], definition))) &&
		(node.components ?
			scope.access(a.value, node.root, definition => types.bind(definition, node.root)) &&
			types.type(node.root, entity) &&
			types.typeArray(node.components.filter((_,index) => index !== node.components.length - 1), entity) &&
			types.bind(node, node.components[node.components.length - 1])
		:
			(node.parent.syntactic_type === "assignment" && ! node.parent.address ?
				scope.accessOrDefine(a.value, node.root, definition => types.bind(definition, node.root)) &&
				types.bind(node, node.root)
			:
				scope.access(a.value, node.root, definition => types.bind(definition, node.root)) &&
				types.bind(node, node.root)
			)
		));
	}


	function shader (node)
	{
		return _
		&&	lexer.assign("name", identifier)
		&& lexer.read(keyword_shader)
		&& scope.define(a.gpu_system, node.name)
		&& lexer.read(keyword_begin)
		&& scope.push()
		&& lexer.loop(_ => lexer.push("statements", gpu_assignment))
		&& lexer.read(keyword_end)
		&& scope.pop()
	}
	function default_gpu_pass (node)
	{
		return true
		&& lexer.loop(_ => lexer.push("statements", gpu_assignment))
	}
	function gpu_pass (node)
	{
		return true
		&& lexer.read(keyword_selection)
		&& lexer.assign("state", gpu_expression)
		&& lexer.read(keyword_begin)
		&& lexer.loop(_ => lexer.push("statements", gpu_assignment))
		&& lexer.read(keyword_end)
	}
	function gpu_assignment (node)
	{
		return _
		&& lexer.assign("address", gpu_assignment_address)
		&& lexer.read(keyword_assign)
		&& lexer.assign("expression", gpu_expression)
		&& types.bind(node.address, node.expression)
	}
	function gpu_expression (node)
	{
		return false
		|| lexer.read(gpu_priority)
		|| lexer.read(gpu_unary)
		|| lexer.read(gpu_binary)
		|| lexer.read(gpu_functional)
		|| lexer.read(gpu_address)
	}
	function gpu_priority (node)
	{
		return true
		&& lexer.read(keyword_open)
		&& lexer.assign("expression", gpu_expression)
		&& lexer.read(keyword_close)
		&& types.bind(node, node.expression)
	}
	function gpu_unary (node)
	{
		return true
		&& lexer.assign("operator", operator)
		&& lexer.assign("expression", gpu_expression)

		&& scope.access(a.unary_operator, node.operator, definition => types.typeArray([node, node.expression], [definition.return_type, ...definition.parameters.map(x => x.type)]))
	}
	function gpu_binary (node)
	{
		return true
		&&	( lexer.assign("left", gpu_priority) || lexer.assign("left", gpu_unary) || lexer.assign("left", gpu_functional) || lexer.assign("left", gpu_address))
		&&	lexer.assign("operator", operator)
		&& lexer.assign("right", gpu_expression)

		&& scope.access(a.binary_operator, node.operator, definition => types.typeArray([node, node.left, node.right], [definition.return_type, ...definition.parameters.map(x => x.type)]))
	}
	function gpu_functional (node)
	{
		return true
		&& lexer.assign("name", identifier)
		&& lexer.read(keyword_open)
		&& lexer.optional(_ => lexer.push("arguments", gpu_expression) && lexer.loop(_ => lexer.read(keyword_next) && lexer.push("arguments", gpu_expression)))
		&& lexer.read(keyword_close)

		&& scope.access(a.gpu_map, node.name, definition => types.typeArray([node, ...(node.arguments||[])], [definition.return_type, ...definition.parameters.map(x => x.type)]))
	}
	function gpu_address (node)
	{
		return false
		|| lexer.read(gpu_material_address)
		|| lexer.read(gpu_vertex_address)
		|| lexer.read(gpu_assignment_address)
	}
	function gpu_assignment_address (node)
	{
		return false
		|| lexer.read(gpu_fragment_address)
		|| lexer.read(gpu_variable)
	}
	function gpu_material_address (node)
	{
		return true
		&& lexer.read(keyword_material)
		&& lexer.read(keyword_access)
		&& lexer.assign("component", identifier)
		&& scope.accessOrDefine(a.material_component, node.component, definition => types.bind(node, definition))
	}
	function gpu_vertex_address (node)
	{
		return true
		&& lexer.read(keyword_vertex)
		&& lexer.read(keyword_access)
		&& lexer.assign("component", identifier)
		&& scope.access(a.vertex_component, node.component, definition => types.bind(node, definition))
	}
	function gpu_fragment_address (node)
	{
		return true
		&& lexer.read(keyword_fragment)
		&& lexer.read(keyword_access)
		&& lexer.assign("component", identifier)
		&& (node.parent.syntactic_type === "gpu_assignment" && ! node.parent.address ?
			scope.accessOrDefine(a.fragment_component, node.component, definition => types.bind(node, definition))
		:
			scope.access(a.fragment_component, node.component, definition => types.bind(node, definition)))
	}
	function gpu_variable (node)
	{
		return true
		&& lexer.assign("name", identifier)
		&& (node.parent.syntactic_type === "gpu_assignment" && ! node.parent.address ?
			scope.accessOrDefine(a.gpu_value, node.name, definition => types.bind(node, definition))
		:
			scope.access(a.gpu_value, node.name, definition => types.bind(node, definition)))
	}
}
